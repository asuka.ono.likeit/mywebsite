<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja" class="h-100">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>>旅行先新規登録画面</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <!-- オリジナルCSS読み込み -->
    <link href="css/common.css" rel="stylesheet">

    <script type="text/javascript">

    function setFocus(){
    	document.aForm.travelName.focus();
    }

    </script>
</head>

<body class="d-flex flex-column h-100" onLoad="setFocus()">

<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
  <a class="navbar-brand" href="ManagementScreenServlet">国内旅行情報サイト(管理者専用画面)</a>
  <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="ナビゲーションの切替">
    <span class="navbar-toggler-icon"></span>
  </button>
  </div>
 </nav>
</header>

  <!-- ページコンテンツ -->
  <main role="main" class="flex-shrink-0">
    <div class="container">
      <div class="mx-auto bg-dark border rounded-lg my-5" style="width:550px;">
  <form name="aForm" method="post" action="NewTravelDestinationServlet" class="mx-auto text-white" style="width:450px;">


	<!-- 旅行先名前のフォーム -->
	<div class="form-group my-5">
	    <label>旅行先名</label>
	    <input name="travelName" type="text" class="form-control" placeholder="旅行先名を入力してください" value="${travelName}">
	</div>


	<!-- 地域の選択欄 -->
	<div class="form-group my-5">
		<label for="Input">地域</label>
		<select name="areaId" class="custom-select">
			<option selected>この選択メニューを開く</option>
		<c:forEach var="areaList" items="${areaList}" >
			<option value="${areaList.id}">${areaList.areaName}</option>
		</c:forEach>
		</select>
	</div>


	<!-- 目的の選択欄 -->
	<div class="form-group my-5">
		<label>目的</label>
		<select name="purposeId" class="custom-select">
			<option selected>この選択メニューを開く</option>
		<c:forEach var="purposeList" items="${purposeList}" >
			<option value="${purposeList.id}">${purposeList.purpose}</option>
		</c:forEach>
		</select>
	</div>


	<!-- 住所のフォーム -->
	<div class="form-group my-5">
	    <label>住所</label>
	    <input name="address" class="form-control" placeholder="住所を入力してください" value="${address}">
	</div>


	<!-- アクセスのフォーム -->
	<div class="form-group my-5">
	    <label>アクセス</label>
	    <input name="access" class="form-control" placeholder="アクセスを入力してください" value="${access}">
	</div>


	<!-- 詳細のフォーム -->
	<div class="form-group my-5">
	    <label>詳細</label>
		<textarea name="detail" cols="40" rows="12" class="form-control" placeholder="詳細を入力してください" ></textarea>
	</div>


	<!-- 画像のフォーム -->
	<div class="form-group my-5">
		<label>画像ファイル</label>
		<p><input name="fileName" type="file" accept="image"></p>
	</div>


	<!-- 登録ボタン -->
	<div class="form-group text-right my-4">
		<button type="submit" class="btn btn-primary">登録</button>
	</div>

   </form>
  </div>
 </div>
</main>

  <!-- フッタ -->
  <footer class="footer mt-auto py-3">
    <div class="container">
      <a class="btn btn-primary" href="javascript:history.back();">一つ前のページへ戻る</a>
    </div>
  </footer>


</body>

</html>