<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja" class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>管理者詳細更新画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<!-- オリジナルCSS読み込み -->
<link href="css/common.css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">

	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="container">
				<a class="navbar-brand" href="ManagementScreenServlet">国内旅行情報サイト(管理者専用画面)</a>
				<button type="button" class="navbar-toggler" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav"
					aria-expanded="false" aria-label="ナビゲーションの切替">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
		</nav>

	</header>

	<!-- ページコンテンツ -->
	<main role="main" class="flex-shrink-0">
	<div class="container">
		<!-- 登録失敗した場合 -->
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger mt-5 text-center h3" role="alert">
				${errMsg}</div>
		</c:if>


		<div class="mx-auto bg-dark border rounded-lg my-5"
			style="width: 550px;">
			<form method="post" action="ManagementDetailUpdateServlet"
				class="mx-auto text-white" style="width: 450px;">


				<!-- 旅行先名前のフォーム -->
				<div class="form-group my-5">
					<label for="Input">旅行先名</label> <input name="travelName"
						type="text" class="form-control" id="Input"
						placeholder="旅行先名を入力してください" value="${travelInfoDetail.travelName}">
				</div>


				<!-- 地域の選択欄 -->
				<div class="form-group my-5">
					<label for="Input">地域</label> <select name="areaId"
						class="custom-select">
						<option selected value="${travelInfoDetail.areaId}">${travelInfoDetail.areaName}</option>
						<c:forEach var="areaList" items="${areaList}">
							<option value="${areaList.id}">${areaList.areaName}</option>
						</c:forEach>
					</select>
				</div>


				<!-- 目的の選択欄 -->
				<div class="form-group my-5">
					<label for="Input">目的</label> <select name="purposeId"
						class="custom-select">
						<option selected value="${travelInfoDetail.purposeId}">${travelInfoDetail.purpose}</option>
						<c:forEach var="purposeList" items="${purposeList}">
							<option value="${purposeList.id}">${purposeList.purpose}</option>
						</c:forEach>
					</select>
				</div>


				<!-- 住所のフォーム -->
				<div class="form-group my-5">
					<label for="Input">住所</label> <input name="address" type="text"
						class="form-control" id="Input" placeholder="住所を入力してください"
						value="${travelInfoDetail.address}">
				</div>


				<!-- アクセスのフォーム -->
				<div class="form-group my-5">
					<label for="Input">アクセス</label> <input name="access" type="text"
						class="form-control" id="Input" placeholder="アクセスを入力してください"
						value="${travelInfoDetail.access}">
				</div>


				<!-- 詳細のフォーム -->
				<div class="form-group my-5">
					<label>詳細</label>
					<textarea name="detail" cols="40" rows="12" class="form-control"
						placeholder="詳細を入力してください">${travelInfoDetail.detail}</textarea>
				</div>


				<!-- 画像のフォーム -->
				<div class="form-group my-5">
					<label>画像ファイル</label>
					<p>
						<input name="fileName" type="file" accept="image"
							value="${travelInfoDetail.fileName}">
					</p>
				</div>


				<!-- 更新ボタン -->
				<div class="form-group text-right my-4">
					<button type="submit" class="btn btn-primary"
						value="${travelInfoDetail.id}" name="id">更新</button>
				</div>

			</form>
		</div>
	</div>
	</main>

	<!-- フッタ -->
	<footer class="footer mt-auto py-3">
		<div class="container">
			<a class="btn btn-primary" href="javascript:history.back();">一つ前のページへ戻る</a>
		</div>
	</footer>


</body>

</html>