<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja" class="h-100">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>管理者画面</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <!-- オリジナルCSS読み込み -->
    <link href="css/common.css" rel="stylesheet">

</head>

<body class="d-flex flex-column h-100">

<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
  <a class="navbar-brand" href="ManagementScreenServlet">国内旅行情報サイト(管理者専用画面)</a>
  <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="ナビゲーションの切替">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
     <!-- 新規旅行先情報登録ボタン -->
      <li class="nav-item">
      	<a href="NewTravelDestinationServlet" class="nav-link">新規旅行先登録</a>
      </li>
     <!-- TOPに戻るボタン -->
      <li class="nav-item">
      	<a href="IndexServlet" class="nav-link">TOPに戻る</a>
      </li>
    </ul>
  </div>
  </div>
</nav>

</header>

  <!-- ページコンテンツ -->
  <main role="main" class="flex-shrink-0">
    <div class="container">
      <!-- 検索結果０件の場合 -->
		<c:if test="${errMsg != null}">
		<script type="text/javascript">

		alert("旅行先が見つかりません")

		</script>
		</c:if>

<div class="mx-auto bg-dark border rounded-lg mt-5" style="width:750px;">
<form method="get" action="ManagementSearchResultServlet" class="mx-auto text-white" style="width:650px;">

	<!-- キーワードのフォーム -->
	<div class="form-group my-4">
	    <label for="Input">キーワード</label>
	    <input name="keyword" type="text" class="form-control" id="Input" placeholder="キーワードを入力してください">
	</div>

	<!-- 地域の選択欄 -->
	<div class="form-group row my-4">
	<div class="col">
		<label for="Input">地域</label>
		<select name="areaId" class="custom-select">
			<option value="">この選択メニューを開く</option>
		<c:forEach var="areaList" items="${areaList}" >
			<option value="${areaList.id}">${areaList.areaName}</option>
		</c:forEach>
		</select>
	</div>

	<!-- 目的の選択欄 -->
	<div class="col">
		<label for="Input">目的</label>
		<select name="purposeId" class="custom-select">
			<option value="">この選択メニューを開く</option>
		<c:forEach var="purposeList" items="${purposeList}" >
			<option value="${purposeList.id}">${purposeList.purpose}</option>
		</c:forEach>
		</select>
	</div>
	</div>

	<!-- 検索ボタン -->
	<div class="form-group text-right my-4">
		<button type="submit" class="btn btn-primary">検索</button>
	</div>

   </form>
  </div>
 </div>
</main>

  <!-- フッタ -->
  <footer class="footer mt-auto py-3">
    <div class="container">
      <a class="btn btn-primary" href="javascript:history.back();">一つ前のページへ戻る</a>
    </div>
  </footer>


</body>

</html>