<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja" class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>国内旅行情報サイト</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="css/slick.css" />
<!-- オリジナルCSS読み込み -->
<link href="css/common.css" rel="stylesheet">

</head>

<body class="d-flex flex-column h-100">

	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="container">
				<a class="navbar-brand" href="IndexServlet">国内旅行情報サイト</a>
				<button type="button" class="navbar-toggler" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav"
					aria-expanded="false" aria-label="ナビゲーションの切替">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav ml-auto">
						<!-- もしログインしていない場合 -->
						<c:if test="${userInfo == null}">
							<li class="nav-item"><a href="LoginServlet" class="nav-link">ログイン</a>
							</li>
							<li class="nav-item"><a href="UserCreateServlet"
								class="nav-link">新規登録</a></li>
						</c:if>

						<!-- もしログインしていた場合 -->

						<!-- 管理者専用画面ボタン -->
						<c:if test="${userInfo.id == 1}">
							<li class="nav-item"><a href="ManagementScreenServlet"
								class="nav-link">管理者専用画面</a></li>
						</c:if>

						<c:if test="${userInfo != null}">
							<li class="nav-item"><a
								href="UserFavoriteServlet?userId=${userInfo.id}"
								class="nav-link">お気に入り一覧</a></li>
							<li class="nav-item"><a
								href="UserUpdateServlet?id=${userInfo.id}" class="nav-link">ユーザ情報更新</a>
							</li>
							<li class="nav-item"><a href="LogoutServlet"
								class="nav-link">ログアウト</a></li>
						</c:if>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<!-- ページコンテンツ -->
	<main role="main" class="flex-shrink-0">
	<div class="container">
		<!-- 検索結果０件の場合 -->
		<c:if test="${errMsg != null}">
			<script type="text/javascript">
				alert("旅行先が見つかりません");
			</script>
		</c:if>

		<div class="mx-auto bg-dark border rounded-lg mt-5"
			style="width: 750px;">
			<form method="get" action="SearchResultServlet"
				class="mx-auto text-white" style="width: 650px;">


				<!-- キーワードのフォーム -->
				<div class="form-group my-4">
					<label for="Input">キーワード</label> <input name="keyword" type="text"
						class="form-control" id="Input" placeholder="キーワードを入力してください">
				</div>


				<div class="form-group row my-4">

					<!-- 地域の選択欄 -->
					<div class="col">
						<label for="Input">地域</label> <select name="areaId"
							class="custom-select">
							<option value="">この選択メニューを開く</option>
							<c:forEach var="areaList" items="${areaList}">
								<option value="${areaList.id}">${areaList.areaName}</option>
							</c:forEach>
						</select>
					</div>

					<!-- 目的の選択欄 -->
					<div class="col">
						<label for="Input">目的</label> <select name="purposeId"
							class="custom-select">
							<option value="">この選択メニューを開く</option>
							<c:forEach var="purposeList" items="${purposeList}">
								<option value="${purposeList.id}">${purposeList.purpose}</option>
							</c:forEach>
						</select>
					</div>

				</div>

				<!-- 検索ボタン -->
				<div class="form-group text-right my-4">
					<button type="submit" class="btn btn-primary">検索</button>
				</div>

			</form>
		</div>

		<!-- オススメの旅行先 -->
		<p class="h3 mt-4">オススメの旅行先</p>
		<div class="row">
			<c:forEach var="rtil" items="${rtil}">
				<div class="col-sm-6 col-md-4 my-4">
					<div class="card img-thumbnail border-white">
						<img class="card-img-top" src="img/${rtil.fileName}" alt="画像"
							width="300" height="200">
						<div class="card-body px-2 py-3 bg-light">
							<h5 class="card-title text-dark">${rtil.travelName}</h5>

							<form method="post" action="TravelDestinationDetailServlet"
								class="mb-0 text-right">
								<!-- ログインしているユーザ情報のid -->
								<input name="userId" type="hidden" value="${userInfo.id}">
								<button name="travelId" type="submit" class="btn btn-primary"
									value="${rtil.id}">詳細</button>
							</form>

						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<!-- /.col-sm-6.col-md-3 -->
			</c:forEach>
		</div>
		<!-- /.row -->



	</div>
	</main>

	<!-- フッタ -->
	<footer class="footer mt-auto py-3">
		<div class="container"></div>
	</footer>


</body>

</html>