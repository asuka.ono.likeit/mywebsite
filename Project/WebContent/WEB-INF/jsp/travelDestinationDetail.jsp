<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja" class="h-100">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>旅行先詳細</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <!-- オリジナルCSS読み込み -->
	<link href="css/common.css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">

<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
  <a class="navbar-brand" href="IndexServlet">国内旅行情報サイト</a>
  <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="ナビゲーションの切替">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
     <!-- もしログインしていない場合 -->
     <c:if test="${userInfo == null}" >
      <li class="nav-item">
      	<a href="LoginServlet" class="nav-link">ログイン</a>
      </li>
      <li class="nav-item">
      	<a href="UserCreateServlet" class="nav-link">新規登録</a>
      </li>
     </c:if>

     <!-- もしログインしていた場合 -->

     <!-- 管理者専用画面ボタン -->
     <c:if test="${userInfo.id == 1}" >
     <li class="nav-item">
      	<a href="ManagementScreenServlet" class="nav-link">管理者専用画面</a>
      </li>
     </c:if>

	 <c:if test="${userInfo != null}" >
	  <li class="nav-item">
      	<a href="UserFavoriteServlet?userId=${userInfo.id}" class="nav-link">お気に入り一覧</a>
      </li>
      <li class="nav-item">
      	<a href="UserUpdateServlet?id=${userInfo.id}" class="nav-link">ユーザ情報更新</a>
      </li>
      <li class="nav-item">
      	<a href="LogoutServlet" class="nav-link">ログアウト</a>
      </li>
	 </c:if>
    </ul>
  </div>
  </div>
</nav>

</header>

  <!-- ページコンテンツ -->
  <main role="main" class="flex-shrink-0">
    <div class="container">
      <div class="row">
				<div class="col-sm-5">
					<p class="h1 mt-4">${travelInfoDetail.travelName}</p>
				</div>

			</div>
			<div class="row">
				<div class="col-sm mt-5">
					<img alt="画像" src="img/${travelInfoDetail.fileName}" width="600"
						height="400">
				</div>
				<div class="col-sm">
					<div class="h5 mt-5">詳細</div>${travelInfoDetail.detail}
					<div class="h5 mt-4">住所</div>${travelInfoDetail.address}
					<div class="h5 mt-4">アクセス</div>${travelInfoDetail.access}</div>
			</div>


			<div class="my-4 float-right form-inline">

				<c:if test="${userInfo != null}">
					<!-- 口コミを書く操作 -->
					<form method="post" action="ReviewWriteServlet" class="mr-2">

						<!-- 口コミを書くボタン -->
						<button type="submit" class="btn btn-primary">口コミを書く</button>

						<!-- ログインしているユーザ情報のid -->
						<input name="userId" type="hidden" value="${userInfo.id}">

						<!-- 旅行先情報のid -->
						<input name="travelId" type="hidden"
							value="${travelInfoDetail.id}">

						<!-- 検索結果 -->
						<input name="keyword" type="hidden" value="${keyword}"> <input
							name="areaId" type="hidden" value="${areaId}"> <input
							name="purposeId" type="hidden" value="${purposeId}">
					</form>
				</c:if>

				<!-- 口コミを見る操作 -->
				<form method="post" action="ReviewReadServlet" class="mr-2">

				<!-- 口コミある場合 -->
				<c:if test="${review != null}">
					<!-- 口コミを見るボタン -->
					<button type="submit" class="btn btn-primary">口コミを見る</button>

					<!-- ログインしているユーザ情報のid -->
					<input name="userId" type="hidden" value="${userInfo.id}">

					<!-- 旅行先情報のid -->
					<input name="travelId" type="hidden" value="${travelInfoDetail.id}">

					<!-- 検索結果 -->
					<input name="keyword" type="hidden" value="${keyword}"> <input
						name="areaId" type="hidden" value="${areaId}"> <input
						name="purposeId" type="hidden" value="${purposeId}">
				</c:if>

				<!-- 口コミない場合 -->
				<c:if test="${review == null}">
					<!-- 口コミを見るボタン -->
					<button type="button" class="btn btn-outline-secondary disabled">口コミがありません</button>
				</c:if>


				</form>

				<c:if test="${userInfo != null}">
					<form method="post" action="FavoriteAddServlet" class="mr-2">

						<!-- お気に入りしていない場合 -->
						<c:if test="${favorite == null}">
							<!-- お気に入りボタン -->
							<button type="submit" class="btn btn-primary">お気に入りに追加</button>

							<!-- ログインしているユーザ情報のid -->
							<input name="userId" type="hidden" value="${userInfo.id}">

							<!-- 旅行先情報のid -->
							<input name="travelId" type="hidden"
								value="${travelInfoDetail.id}">

							<!-- 検索結果 -->
							<input name="keyword" type="hidden" value="${keyword}">
							<input name="areaId" type="hidden" value="${areaId}">
							<input name="purposeId" type="hidden" value="${purposeId}">

						</c:if>

						<!-- もしお気に入りしていた場合 -->
						<c:if test="${favorite != null}">
							<button type="button" class="btn btn-outline-secondary" disabled>お気に入り済み</button>
						</c:if>

					</form>
				</c:if>
			</div>
		</div>
  </main>

  <!-- フッタ -->
  <footer class="footer mt-auto py-3">
    <div class="container">
      <a class="btn btn-primary" href="javascript:history.back();">一つ前のページへ戻る</a>
    </div>
  </footer>


</body>

</html>