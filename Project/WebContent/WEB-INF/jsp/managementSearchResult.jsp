<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja" class="h-100">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>管理者検索結果画面</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <!-- オリジナルCSS読み込み -->
    <link href="css/common.css" rel="stylesheet">

    <!-- 削除ボタンの確認ダイアログ -->
    <script type="text/javascript">

		function checkSubmit(){

			flag = confirm("本当に削除しますか？");
			return flag;
		}
	</script>
</head>

<body class="d-flex flex-column h-100">

<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
  <a class="navbar-brand" href="ManagementScreenServlet">国内旅行情報サイト(管理者専用画面)</a>
  <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="ナビゲーションの切替">
    <span class="navbar-toggler-icon"></span>
  </button>
  </div>
</nav>

</header>

  <!-- ページコンテンツ -->
  <main role="main" class="flex-shrink-0">
    <div class="container">
      <!-- 検索結果 -->
 <div class="row mt-4">
	<c:forEach var="travelInfoList" items="${travelInfoList}" >
	  <div class="col-sm-6 col-md-4 mt-5">
	    <div class="card img-thumbnail border-white">
	      <img class="card-img-top" src="img/${travelInfoList.fileName}" alt="画像" width="300" height="200">
	      <div class="card-body px-2 py-3 bg-light">
	        <h5 class="card-title text-dark">${travelInfoList.travelName}</h5>

		<div class="row float-right">
	        <form method="get" action="ManagementDetailUpdateServlet" class="mr-2 mb-0 text-right">
	         <button name="id" type="submit" class="btn btn-primary" value="${travelInfoList.id}">更新</button>
	        </form>

	        <form method="post" action="ManagementDetailDeleteServlet" class="mr-3 mb-0 text-right" onSubmit="return checkSubmit()">

			 <!-- 検索結果から削除行って戻るボタン押した時に検索結果を出すための情報 -->
			 <input name="keyword" type="hidden" value="${keyword}">
			 <input name="areaId" type="hidden" value="${areaId}">
			 <input name="purposeId" type="hidden" value="${purposeId}">

			 <button name="id" type="submit" class="btn btn-danger" value="${travelInfoList.id}">削除</button>
			</form>
		</div>

	      </div><!-- /.card-body -->
	    </div><!-- /.card -->
	  </div><!-- /.col-sm-6.col-md-3 -->
	 </c:forEach>
 </div><!-- /.row -->


<!-- 検索結果2ページしかない時 -->
<c:if test="${pageMax == 2}">

	<!-- 検索結果1ページ目の時 -->
	<c:if test="${pageNum == 1}">
	<nav aria-label="ページ送りの実例">
	  <ul class="pagination justify-content-center mt-4">
	    <li class="page-item"><a class="page-link" href="#">${pageNum}</a></li>
	    <li class="page-item"><a class="page-link" href="ManagementSearchResultServlet?pageNum=${pageNum + 1}&keyword=${keyword}&areaId=${areaId}&purposeId=${purposeId}">${pageNum + 1}</a></li>
	  </ul>
	</nav>
	</c:if>

	<!-- 検索結果2ページ目の時 -->
	<c:if test="${pageNum == 2}">
		<nav aria-label="ページ送りの実例">
		  <ul class="pagination justify-content-center mt-4">
		    <li class="page-item"><a class="page-link" href="ManagementSearchResultServlet?pageNum=${pageNum - 1}&keyword=${keyword}&areaId=${areaId}&purposeId=${purposeId}">${pageNum - 1}</a></li>
		    <li class="page-item"><a class="page-link" href="#">${pageNum}</a></li>
		  </ul>
		</nav>
	</c:if>
</c:if>


<!-- 検索結果3ページ以上の時 -->
<c:if test="${pageMax >= 3}">

	<!-- 1ページ目（最初）の時 -->
	<c:if test="${pageNum == 1}">
	<nav aria-label="ページ送りの実例">
	  <ul class="pagination justify-content-center mt-4">
	    <li class="page-item"><a class="page-link" href="#">${pageNum}</a></li>
	    <li class="page-item"><a class="page-link" href="ManagementSearchResultServlet?pageNum=${pageNum + 1}&keyword=${keyword}&areaId=${areaId}&purposeId=${purposeId}">${pageNum + 1}</a></li>
	    <li class="page-item"><a class="page-link" href="ManagementSearchResultServlet?pageNum=${pageNum + 2}&keyword=${keyword}&areaId=${areaId}&purposeId=${purposeId}">${pageNum + 2}</a></li>
	  </ul>
	</nav>
	</c:if>

	<!-- 真ん中のページ目の時 -->
	<c:if test="${pageNum != pageMax && pageNum != 1}">
	<nav aria-label="ページ送りの実例">
	  <ul class="pagination justify-content-center mt-4">
	    <li class="page-item"><a class="page-link" href="ManagementSearchResultServlet?pageNum=${pageNum - 1}&keyword=${keyword}&areaId=${areaId}&purposeId=${purposeId}">${pageNum - 1}</a></li>
	    <li class="page-item"><a class="page-link" href="#">${pageNum}</a></li>
	    <li class="page-item"><a class="page-link" href="ManagementSearchResultServlet?pageNum=${pageNum + 1}&keyword=${keyword}&areaId=${areaId}&purposeId=${purposeId}">${pageNum + 1}</a></li>
	  </ul>
	</nav>
	</c:if>

	<!-- 最後のページ目の時 -->
	<c:if test="${pageNum == pageMax}">
	<nav aria-label="ページ送りの実例">
	  <ul class="pagination justify-content-center mt-4">
	    <li class="page-item"><a class="page-link" href="ManagementSearchResultServlet?pageNum=${pageNum - 2}&keyword=${keyword}&areaId=${areaId}&purposeId=${purposeId}">${pageNum - 2}</a></li>
	    <li class="page-item"><a class="page-link" href="ManagementSearchResultServlet?pageNum=${pageNum - 1}&keyword=${keyword}&areaId=${areaId}&purposeId=${purposeId}">${pageNum - 1}</a></li>
	    <li class="page-item"><a class="page-link" href="#">${pageNum}</a></li>
	  </ul>
	</nav>
	</c:if>

  </c:if>
 </div>
</main>

  <!-- フッタ -->
  <footer class="footer mt-auto py-3">
    <div class="container">
      <a class="btn btn-primary" href="javascript:history.back();">一つ前のページへ戻る</a>
    </div>
  </footer>


</body>

</html>