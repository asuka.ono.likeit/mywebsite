<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja" class="h-100">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>口コミ一覧画面</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <!-- オリジナルCSS読み込み -->
    <link href="css/common.css" rel="stylesheet">

    <!-- 削除ボタンの確認ダイアログ -->
    <script type="text/javascript">

		function checkSubmit(){

			flag = confirm("本当に削除しますか？");
			return flag;
		}
	</script>
</head>

<body class="d-flex flex-column h-100">

<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
  <a class="navbar-brand" href="IndexServlet">国内旅行情報サイト</a>
  <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="ナビゲーションの切替">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
     <!-- もしログインしていない場合 -->
     <c:if test="${userInfo == null}" >
      <li class="nav-item">
      	<a href="LoginServlet" class="nav-link">ログイン</a>
      </li>
      <li class="nav-item">
      	<a href="UserCreateServlet" class="nav-link">新規登録</a>
      </li>
     </c:if>

     <!-- もしログインしていた場合 -->

     <!-- 管理者専用画面ボタン -->
     <c:if test="${userInfo.id == 1}" >
     <li class="nav-item">
      	<a href="ManagementScreenServlet" class="nav-link">管理者専用画面</a>
      </li>
     </c:if>

	 <c:if test="${userInfo != null}" >
	  <li class="nav-item">
      	<a href="UserFavoriteServlet?userId=${userInfo.id}" class="nav-link">お気に入り一覧</a>
      </li>
      <li class="nav-item">
      	<a href="UserUpdateServlet?id=${userInfo.id}" class="nav-link">ユーザ情報更新</a>
      </li>
      <li class="nav-item">
      	<a href="LogoutServlet" class="nav-link">ログアウト</a>
      </li>
	 </c:if>
    </ul>
  </div>
  </div>
</nav>

</header>


  <!-- ページコンテンツ -->
  <main role="main" class="flex-shrink-0">
    <div class="container">
      <!-- 口コミない場合 -->
		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger mt-5 text-center h3" role="alert">
		  ${errMsg}
		</div>
		</c:if>


	<form method="post" action="ReviewDeleteServlet" onSubmit="return checkSubmit()">
	  <c:forEach var="reviewList" items="${reviewList}">
	   <!-- 口コミid -->
	   <input name="reviewId" type="hidden" value="${reviewList.id}">
	   <!-- 旅行先id -->
	   <input name="travelId" type="hidden" value="${travelId}">

		<div class="card card-body bg-light my-4">
		  <p>${reviewList.formatDate}</p>
		  ${reviewList.review}
		  <div class="form-group text-right">
		  ${reviewList.userName}さん
		  <c:if test="${reviewList.userName == userInfo.name || userInfo.name == 'admin'}">
		  <button type="submit" class="btn btn-danger">削除</button>
		  </c:if>
		  </div>
		</div>
	  </c:forEach>
	</form>
  </div>
</main>

  <!-- フッタ -->
  <footer class="footer mt-auto py-3">
    <div class="container">
      <a class="btn btn-primary" href="javascript:history.back();">一つ前のページへ戻る</a>
    </div>
  </footer>


</body>

</html>