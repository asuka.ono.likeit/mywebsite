<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja" class="h-100">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>新規登録画面</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <!-- オリジナルCSS読み込み -->
    <link href="css/common.css" rel="stylesheet">

    <script type="text/javascript">

    function setFocus(){
    	document.aForm.userName.focus();
    }

    </script>
</head>

<body class="d-flex flex-column h-100" onLoad="setFocus()">

<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
  <a class="navbar-brand" href="IndexServlet">国内旅行情報サイト</a>
  <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="ナビゲーションの切替">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
     <!-- もしログインしていない場合 -->
     <c:if test="${userInfo == null}" >
      <li class="nav-item">
      	<a href="LoginServlet" class="nav-link">ログイン</a>
      </li>
      <li class="nav-item">
      	<a href="UserCreateServlet" class="nav-link">新規登録</a>
      </li>
     </c:if>

     <!-- もしログインしていた場合 -->
	 <c:if test="${userInfo != null}" >
	  <li class="nav-item">
      	<a href="UserFavoriteServlet?userId=${userInfo.id}" class="nav-link">お気に入り一覧</a>
      </li>
	 </c:if>
    </ul>
  </div>
  </div>
</nav>

</header>

  <!-- ページコンテンツ -->
  <main role="main" class="flex-shrink-0">
    <div class="container">
      <!-- 登録失敗した場合 -->
		<c:if test="${errMsg != null}">
		  <script type="text/javascript">
			alert("登録失敗しました");
		  </script>
		</c:if>

<div class="mx-auto bg-dark border rounded-lg my-5" style="width:550px;">
<form name="aForm" method="post" action="UserCreateServlet" class="mx-auto text-white" style="width:450px;">

	<!-- 名前のフォーム -->
	<div class="form-group my-5">
	    <label for="Input">名前</label>
	    <input name="userName" type="text" class="form-control" id="Input" placeholder="名前を入力してください">
	  </div>

	 <!-- 生年月日のフォーム -->
	<div class="form-group my-5">
	    <label for="Input">生年月日</label>
	    <input name="birthDate" type="text" class="form-control" id="Input" placeholder="YYYY-MM-DDの形式で入力してください">
	  </div>

	<!-- 住所のフォーム -->
	<div class="form-group my-5">
	    <label for="Input">住所</label>
	    <input name="address" type="text" class="form-control" id="Input" placeholder="住所を入力してください">
	  </div>


	<!-- ログインIDのフォーム -->
	<div class="form-group my-5">
	    <label for="Input">ログインID</label>
	    <input name="loginId" type="text" class="form-control" id="Input" placeholder="ログインIDを入力してください">
	  </div>

	  <!-- passwordのフォーム -->
	  <div class="form-group my-5">
	    <label for="Input">Password</label>
	    <input name="password" type="password" class="form-control" id="Input" placeholder="Password">
	  </div>

	  <!-- password（確認）のフォーム -->
	  <div class="form-group my-5">
	    <label for="Input">Password（確認）</label>
	    <input name="passwordConf" type="password" class="form-control" id="Input" placeholder="Password（確認）">
	  </div>

	  <!-- 登録ボタン -->
	<div class="form-group text-right my-4">
	<button type="submit" class="btn btn-primary">登録</button>
	</div>

   </form>
  </div>
 </div>
</main>

  <!-- フッタ -->
  <footer class="footer mt-auto py-3">
    <div class="container">
      <a class="btn btn-primary" href="javascript:history.back();">一つ前のページへ戻る</a>
    </div>
  </footer>


</body>

</html>