package model;

import java.io.Serializable;

public class AreaTable implements Serializable{

	private int id;
	private String areaName;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

}
