package model;

import java.io.Serializable;
import java.util.Date;

	public class User implements Serializable {
		private int id;
		private String name;
		private String address;
		private Date birthDate;
		private String loginId;
		private String password;
		private String createDate;
		private String updateDate;

		public User(String loginId, String name) {
			this.loginId = loginId;
			this.name = name;
		}



		// ログインセッションを保存するためのコンストラクタ
		public User(int id, String loginId, String name) {
			this.id = id;
			this.loginId = loginId;
			this.name = name;
		}

		// 全てのデータをセットするコンストラクタ
		public User(int id, String name, String address, Date birthDate, String loginId, String password, String createDate,
				String updateDate) {
			this.id = id;
			this.name = name;
			this.address = address;
			this.birthDate = birthDate;
			this.loginId = loginId;
			this.password = password;
			this.createDate = createDate;
			this.updateDate = updateDate;
		}



		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public Date getBirthDate() {
			return birthDate;
		}
		public void setBirthDate(Date birthDate) {
			this.birthDate = birthDate;
		}
		public String getLoginId() {
			return loginId;
		}
		public void setLoginId(String loginId) {
			this.loginId = loginId;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getCreateDate() {
			return createDate;
		}
		public void setCreateDate(String createDate) {
			this.createDate = createDate;
		}
		public String getUpdateDate() {
			return updateDate;
		}
		public void setUpdateDate(String updateDate) {
			this.updateDate = updateDate;
		}


}
