package model;

import java.io.Serializable;

public class Favorite implements Serializable{

	private int id;
	private int userId;
	private int travelId;
	private String createDate;

	private String travelName;
	private int areaId;
	private int purposeId;
	private String address;
	private String access;
	private String detail;
	private String fileName;




	//id取得するためのコンストラクタ
		public Favorite(int id) {
			this.id = id;
		}





	public Favorite() {
			// TODO 自動生成されたコンストラクター・スタブ
		}





	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getTravelId() {
		return travelId;
	}
	public void setTravelId(int travelId) {
		this.travelId = travelId;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}





	public String getTravelName() {
		return travelName;
	}





	public void setTravelName(String travelName) {
		this.travelName = travelName;
	}





	public int getAreaId() {
		return areaId;
	}





	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}





	public int getPurposeId() {
		return purposeId;
	}





	public void setPurposeId(int purposeId) {
		this.purposeId = purposeId;
	}





	public String getAddress() {
		return address;
	}





	public void setAddress(String address) {
		this.address = address;
	}





	public String getAccess() {
		return access;
	}





	public void setAccess(String access) {
		this.access = access;
	}





	public String getDetail() {
		return detail;
	}





	public void setDetail(String detail) {
		this.detail = detail;
	}





	public String getFileName() {
		return fileName;
	}





	public void setFileName(String fileName) {
		this.fileName = fileName;
	}





}
