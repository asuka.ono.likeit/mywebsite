package model;

import java.io.Serializable;

public class PurposeTable implements Serializable{

	private int id;
	private String purpose;


	//id取得するためのコンストラクタ
	public PurposeTable(int id,String purpose) {
		this.id = id;
		this.purpose = purpose;
	}


	public PurposeTable() {
		// TODO 自動生成されたコンストラクター・スタブ
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

}
