package model;

import java.io.Serializable;

	public class TravelInfo implements Serializable {

		private int id;
		private String travelName;
		private int areaId;
		private int purposeId;
		private String address;
		private String access;
		private String detail;
		private String fileName;
		private String createDate;
		private String updateDate;


		private String areaName;
		private String purpose;



		public TravelInfo(int id,String travelName,int areaId,int purposeId,String address,
				String access,String detail,String fileName,String createDate,String updateDate) {

			this.setId(id);
			this.setTravelName(travelName);
			this.setAreaId(areaId);
			this.setPurposeId(purposeId);
			this.setAddress(address);
			this.setAccess(access);
			this.setDetail(detail);
			this.setFileName(fileName);
			this.setCreateDate(createDate);
			this.setUpdateDate(updateDate);
		}



		public TravelInfo(int id,String travelName,String areaName,int areaId,String purpose,int purposeId,String address,
				String access,String detail,String fileName,String createDate,String updateDate) {

			this.setId(id);
			this.setTravelName(travelName);
			this.setAreaName(areaName);
			this.setAreaId(areaId);
			this.setPurpose(purpose);
			this.setPurposeId(purposeId);
			this.setAddress(address);
			this.setAccess(access);
			this.setDetail(detail);
			this.setFileName(fileName);
			this.setCreateDate(createDate);
			this.setUpdateDate(updateDate);
		}



		public TravelInfo() {

		}



		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getTravelName() {
			return travelName;
		}
		public void setTravelName(String travelName) {
			this.travelName = travelName;
		}
		public int getAreaId() {
			return areaId;
		}
		public void setAreaId(int areaId) {
			this.areaId = areaId;
		}
		public int getPurposeId() {
			return purposeId;
		}
		public void setPurposeId(int purposeId) {
			this.purposeId = purposeId;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getAccess() {
			return access;
		}
		public void setAccess(String access) {
			this.access = access;
		}
		public String getDetail() {
			return detail;
		}
		public void setDetail(String detail) {
			this.detail = detail;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public String getCreateDate() {
			return createDate;
		}
		public void setCreateDate(String createDate) {
			this.createDate = createDate;
		}
		public String getUpdateDate() {
			return updateDate;
		}
		public void setUpdateDate(String updateDate) {
			this.updateDate = updateDate;
		}



		public String getAreaName() {
			return areaName;
		}



		public void setAreaName(String areaName) {
			this.areaName = areaName;
		}



		public String getPurpose() {
			return purpose;
		}



		public void setPurpose(String purpose) {
			this.purpose = purpose;
		}



}
