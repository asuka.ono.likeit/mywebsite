package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TravelInfoDao;
import model.TravelInfo;

/**
 * Servlet implementation class ManagementDetailDeleteServlet
 */
@WebServlet("/ManagementDetailDeleteServlet")
public class ManagementDetailDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManagementDetailDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");


		// 入力項目を取得
        String id = request.getParameter("id");

        // 入力項目を引数に渡して、Daoのメソッドを実行
     	TravelInfoDao travelInfoDao = new TravelInfoDao();
     	TravelInfo travelInfo = travelInfoDao.travelInfoDelete(id);


     	// 入力項目を取得
     	String keyword = request.getParameter("keyword");
     	String areaId = request.getParameter("areaId");
     	String purposeId = request.getParameter("purposeId");


     	//削除終わった時に使う
		request.setAttribute("keyword", keyword);
		request.setAttribute("areaId", areaId);
		request.setAttribute("purposeId", purposeId);


     	// ManagementSearchResultにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("ManagementSearchResultServlet");
		dispatcher.forward(request, response);



	}

}
