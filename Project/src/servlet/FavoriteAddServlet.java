package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.FavoriteDao;
import model.Favorite;

/**
 * Servlet implementation class FavoriteAddServlet
 */
@WebServlet("/FavoriteAddServlet")
public class FavoriteAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FavoriteAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");


		// 入力項目を取得
		String userId = request.getParameter("userId");
		String travelId = request.getParameter("travelId");


		// 入力項目を引数に渡して、Daoのメソッドを実行
     	FavoriteDao favoriteDao = new FavoriteDao();
     	Favorite favorite = favoriteDao.favoriteAdd(userId,travelId);


     	// 検索結果を取得
     	String keyword = request.getParameter("keyword");
     	String areaId = request.getParameter("areaId");
     	String purposeId = request.getParameter("purposeId");

     	//お気に入り登録終わった時に使う
     	request.setAttribute("keyword", keyword);
     	request.setAttribute("areaId", areaId);
     	request.setAttribute("purposeId", purposeId);
     	request.setAttribute("travelId", travelId);
     	request.setAttribute("userId", userId);


     	// TravelDestinationDetailServletにフォワード
     	RequestDispatcher dispatcher = request.getRequestDispatcher("TravelDestinationDetailServlet");
     	dispatcher.forward(request, response);


	}

}
