package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TravelInfoDao;
import model.TravelInfo;

/**
 * Servlet implementation class SearchResultServlet
 */
@WebServlet("/SearchResultServlet")
public class SearchResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchResultServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// リクエストパラメータの文字コードを指定
	        request.setCharacterEncoding("UTF-8");


			// 入力項目を取得
			String keyword = request.getParameter("keyword");
			String areaId = request.getParameter("areaId");
			String purposeId = request.getParameter("purposeId");

			//1ページに表示する旅行先数
			final int maxTravelCount = 6;

			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer.parseInt(request.getParameter("pageNum") == null ? "1" : request.getParameter("pageNum"));

			// 商品リストを取得 ページ表示分のみ
			ArrayList<TravelInfo> travelInfoList = TravelInfoDao.getTravelInfoList(keyword,areaId,purposeId,pageNum,maxTravelCount);

			// 検索ワードに対しての総ページ数を取得
			double travelCount = TravelInfoDao.getTravelCount(keyword,areaId,purposeId);
			int pageMax = (int) Math.ceil(travelCount / maxTravelCount);

			//総アイテム数
			request.setAttribute("travelCount", (int) travelCount);
			// 総ページ数
			request.setAttribute("pageMax", pageMax);
			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("travelInfoList", travelInfoList);


			//検索結果0件の場合
			if(travelInfoList == null || travelInfoList.size() == 0) {

				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "旅行先が見つかりません");

				//オススメの旅行先の情報を取得
				try {
					ArrayList<TravelInfo> recommendedTravelInfoList = TravelInfoDao.getAllTravelInfoList();
					request.setAttribute("rtil", recommendedTravelInfoList);
				} catch (SQLException e) {
					e.printStackTrace();
				}

				// index.jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
				dispatcher.forward(request, response);
				return;
			}


			//検索結果がある場合

				//戻るボタンの時に使う
				request.setAttribute("keyword", keyword);
				request.setAttribute("areaId", areaId);
				request.setAttribute("purposeId", purposeId);

				// searchResult.jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/searchResult.jsp");
				dispatcher.forward(request, response);


		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

}
