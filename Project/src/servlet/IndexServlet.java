package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AreaDao;
import dao.PurposeDao;
import dao.TravelInfoDao;
import model.AreaTable;
import model.PurposeTable;
import model.TravelInfo;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		//地域の情報を取得
		try {
			ArrayList<AreaTable> areaList = AreaDao.getAreaList();
			session.setAttribute("areaList", areaList);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}


		//目的の情報を取得
		try {
			ArrayList<PurposeTable> purposeList = PurposeDao.getPurposeList();
			session.setAttribute("purposeList", purposeList);
		} catch (SQLException e) {
			e.printStackTrace();
		}


		//オススメの旅行先の情報を取得
		try {
			ArrayList<TravelInfo> recommendedTravelInfoList = TravelInfoDao.getAllTravelInfoList();
			request.setAttribute("rtil", recommendedTravelInfoList);
		} catch (SQLException e) {
			e.printStackTrace();
		}



		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
