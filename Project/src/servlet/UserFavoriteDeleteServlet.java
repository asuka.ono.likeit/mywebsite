package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.FavoriteDao;
import model.Favorite;

/**
 * Servlet implementation class UserFavoriteDeleteServlet
 */
@WebServlet("/UserFavoriteDeleteServlet")
public class UserFavoriteDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserFavoriteDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");


		// ユーザidと旅行先idを取得
		String userId = request.getParameter("userId");
		String travelId = request.getParameter("travelId");


		// 入力項目を引数に渡して、Daoのメソッドを実行
     	FavoriteDao favoriteDao = new FavoriteDao();
     	Favorite travelInfo = favoriteDao.userFavoriteDelete(userId,travelId);


     	// UserFavoriteServletにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("UserFavoriteServlet");
		dispatcher.forward(request, response);
		return;



	}

}
