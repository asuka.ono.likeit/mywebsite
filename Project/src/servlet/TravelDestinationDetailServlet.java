package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.FavoriteDao;
import dao.ReviewDao;
import dao.TravelInfoDao;
import model.Favorite;
import model.Review;
import model.TravelInfo;

/**
 * Servlet implementation class TravelDestinationDetailServlet
 */
@WebServlet("/TravelDestinationDetailServlet")
public class TravelDestinationDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TravelDestinationDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/**検索結果の保持*/
		// 検索結果を受け取る
		String keyword = request.getParameter("keyword");
		String areaId = request.getParameter("areaId");
		String purposeId = request.getParameter("purposeId");

		// 検索結果をリクエストスコープにセット
		request.setAttribute("keyword", keyword);
		request.setAttribute("areaId", areaId);
		request.setAttribute("purposeId", purposeId);


		/**旅行先情報の取得*/
		// 旅行先idを受け取る
		String travelId = request.getParameter("travelId");

		// idを引数にして、idに紐づく旅行先情報を出力する
		TravelInfoDao travelInfoDao = new TravelInfoDao();
		TravelInfo travelInfoDetail = travelInfoDao.travelInfoDetail(travelId);

		// 旅行先情報をリクエストスコープにセット
		request.setAttribute("travelInfoDetail", travelInfoDetail);


		/**お気に入り情報の取得*/
		//ユーザidの取得
		String userId = request.getParameter("userId");

		// ユーザidと旅行先idからお気に入り情報を取得
		FavoriteDao favoriteDao = new FavoriteDao();
     	Favorite favorite = favoriteDao.favoriteConf(userId,travelId);

     	// お気に入り情報をリクエストスコープにセット
     	request.setAttribute("favorite", favorite);


     	/**口コミ情報の取得*/
     	//旅行先idから口コミがあるかどうか判断する
			ReviewDao reviewDao = new ReviewDao();
			Review review = ReviewDao.reviewConf(travelId);

		// 口コミ情報をリクエストスコープにセット
	     	request.setAttribute("review", review);


		// travelDestinationDetail.jspにフォワード
     	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/travelDestinationDetail.jsp");
     	dispatcher.forward(request, response);
     	return;

	}

}
