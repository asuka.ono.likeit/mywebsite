package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ReviewDao;
import model.Review;

/**
 * Servlet implementation class ReviewDeleteServlet
 */
@WebServlet("/ReviewDeleteServlet")
public class ReviewDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		// 旅行先idの取得
		String travelId = request.getParameter("travelId");

		// 旅行先idをリクエストスコープにセット
		request.setAttribute("travelId", travelId);


		/**口コミ情報を削除*/
		// reviewIdの取得
		String reviewId = request.getParameter("reviewId");

		//reviewIdから口コミ情報を削除
		ReviewDao reviewDao = new ReviewDao();
		Review review = reviewDao.reviewDelete(reviewId);

		// ReviewReadServletにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("ReviewReadServlet");
		dispatcher.forward(request, response);
	}

}
