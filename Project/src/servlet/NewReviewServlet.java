package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ReviewDao;
import model.Review;

/**
 * Servlet implementation class NewReviewServlet
 */
@WebServlet("/NewReviewServlet")
public class NewReviewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewReviewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");


		// ユーザidと旅行先idと入力内容の取得
        String userId = request.getParameter("userId");
		String travelId = request.getParameter("travelId");
		String review = request.getParameter("review");


		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
     	ReviewDao reviewDao = new ReviewDao();
     	Review Review = reviewDao.setReview(userId,travelId,review);


     	// 旅行先idをリクエストスコープにセット
        request.setAttribute("travelId", travelId);


     	if(review.equals("")) {

     		// リクエストスコープにエラーメッセージをセット
     		request.setAttribute("errMsg", "口コミが記入されていません");

     		// reviewWrite.jspにフォワード
     		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reviewWrite.jsp");
     		dispatcher.forward(request, response);
     		return;

     	}


     	// travelDestinationDetail.jspにフォワード
     	RequestDispatcher dispatcher = request.getRequestDispatcher("TravelDestinationDetailServlet");
     	dispatcher.forward(request, response);
     	return;
	}


}
