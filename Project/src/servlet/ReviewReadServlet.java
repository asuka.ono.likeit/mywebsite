package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ReviewDao;
import model.Review;

/**
 * Servlet implementation class ReviewReadServlet
 */
@WebServlet("/ReviewReadServlet")
public class ReviewReadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewReadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// 旅行先idの取得
		String travelId = request.getParameter("travelId");

		// 旅行先idをリクエストスコープにセット
		request.setAttribute("travelId", travelId);

		//ユーザidの取得
		String userId = request.getParameter("userId");

		// ユーザidをリクエストスコープにセット
		request.setAttribute("userId", userId);


		/**Review情報の取得*/
		try {
			//旅行先idからユーザid・本文・投稿日時を取得
			ArrayList<Review> reviewList = ReviewDao.getReviewList(travelId);

			// 口コミ情報をリクエストスコープにセット
			request.setAttribute("reviewList", reviewList);

			/**口コミがない場合*/
			if (reviewList == null || reviewList.size() == 0) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "口コミはありません");

			// TravelDestinationDetailServletにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("TravelDestinationDetailServlet");
			dispatcher.forward(request, response);
			return;

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}



		// reviewRead.jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reviewRead.jsp");
		dispatcher.forward(request, response);


	}

}
