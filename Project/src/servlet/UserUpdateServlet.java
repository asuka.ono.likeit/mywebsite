package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		User userDetail = userDao.userDetail(id);

		// ユーザ情報をリクエストスコープにセット
		request.setAttribute("userDetail", userDetail);

		// ユーザ更新のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // リクエストパラメータの入力項目を取得
        String id = request.getParameter("id");
        String userName = request.getParameter("userName");
        String address = request.getParameter("address");
     	String birthDate= request.getParameter("birthDate");
        String loginId = request.getParameter("loginId");
     	String password = request.getParameter("password");
    	String passwordConf = request.getParameter("passwordConf");



     	//更新失敗の場合
     	if(!password.equals(passwordConf) || userName.equals("") || address.equals("") || birthDate.equals("")
     			|| loginId.equals("") || password.equals("") || passwordConf.equals("")) {

	     	// リクエストスコープにエラーメッセージをセット
	     	request.setAttribute("errMsg", "入力した内容は正しくありません");

	     	// リクエストスコープに最初の情報をセット
	     	request.setAttribute("userName", userName);
	     	request.setAttribute("address", address);
	     	request.setAttribute("birthDate", birthDate);
	     	request.setAttribute("loginId", loginId);

	     	// userUpdate.jspにフォワード
	     	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
	     	dispatcher.forward(request, response);
	     	return;

     	}

     	//更新成功の場合

     	// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
     	UserDao userInfoDao = new UserDao();
     	User user = userInfoDao.updateUserInfo(id,userName,address,birthDate,loginId,password);


     	// ユーザ一覧のサーブレットにリダイレクト
     		response.sendRedirect("IndexServlet");



     }


}


