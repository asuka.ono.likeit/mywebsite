package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TravelInfoDao;
import model.TravelInfo;

/**
 * Servlet implementation class ManagementDetailUpdateServlet
 */
@WebServlet("/ManagementDetailUpdateServlet")
public class ManagementDetailUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManagementDetailUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		// URLからGETパラメータとしてreturnIndexServletを受け取る
		String keyword = request.getParameter("keyword");
		String areaId = request.getParameter("areaId");
		String purposeId = request.getParameter("purposeId");

		// returnIndexServletをリクエストスコープにセット
		request.setAttribute("keyword", keyword);
		request.setAttribute("areaId", areaId);
		request.setAttribute("purposeId", purposeId);


		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");


		// idを引数にして、idに紐づく旅行先情報を出力する
		TravelInfoDao travelInfoDao = new TravelInfoDao();
		TravelInfo travelInfoDetail = travelInfoDao.travelInfoDetail(id);

		// 旅行先情報をリクエストスコープにセット
		request.setAttribute("travelInfoDetail", travelInfoDetail);



		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/managementDetailUpdate.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");


		// 入力項目を取得
        String id = request.getParameter("id");
		String travelName = request.getParameter("travelName");
		String areaId = request.getParameter("areaId");
		String purposeId = request.getParameter("purposeId");
		String address = request.getParameter("address");
		String access = request.getParameter("access");
		String detail = request.getParameter("detail");
		String fileName = request.getParameter("fileName");


		// 入力項目を引数に渡して、Daoのメソッドを実行
		TravelInfoDao travelInfoDao = new TravelInfoDao();
		TravelInfo travelInfo = travelInfoDao.travelInfoUpdate(id,travelName,areaId,purposeId,address,access,detail,fileName);


		/** 登録できない場合 **/
		if (travelName.equals("") || areaId.equals("") || purposeId.equals("") ||
				address.equals("") || access.equals("") || detail.equals("") || fileName.equals("")) {

		// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力した内容は正しくありません");

		// managementDetailUpdate.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/managementDetailUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}


		/** 登録できる場合 **/

		// managemantScreen.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/managementScreen.jsp");
			dispatcher.forward(request, response);
			return;



	}

}
