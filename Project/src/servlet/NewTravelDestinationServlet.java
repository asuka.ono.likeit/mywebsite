package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TravelInfoDao;
import model.TravelInfo;

/**
 * Servlet implementation class NewTravelDestinationServlet
 */
@WebServlet("/NewTravelDestinationServlet")
public class NewTravelDestinationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewTravelDestinationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newTravelDestination.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");


		// 入力項目を取得
		String travelName = request.getParameter("travelName");
		String areaId = request.getParameter("areaId");
		String purposeId = request.getParameter("purposeId");
		String address = request.getParameter("address");
		String access = request.getParameter("access");
		String detail = request.getParameter("detail");
		String fileName = request.getParameter("fileName");


		// 入力項目を引数に渡して、Daoのメソッドを実行
		TravelInfoDao travelInfoDao = new TravelInfoDao();
		TravelInfo travelInfo = travelInfoDao.NewTravelInfo(travelName,areaId,purposeId,address,access,detail,fileName);


		/** 登録できない場合 **/
		if (travelName.equals("") || areaId.equals("") || purposeId.equals("") ||
				address.equals("") || access.equals("") || detail.equals("") || fileName.equals("")) {

		// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力した内容は正しくありません");

		// リクエストスコープに入力した値をセット
			request.setAttribute("travelName",travelName);
			request.setAttribute("birthDate",address);
			request.setAttribute("access",access);
			request.setAttribute("fileName",fileName);

		// newTravelDestination.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newTravelDestination.jsp");
			dispatcher.forward(request, response);
			return;
		}


		/** 登録できる場合 **/

		// newTravelDestination.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/managementScreen.jsp");
			dispatcher.forward(request, response);
			return;



	}

}
