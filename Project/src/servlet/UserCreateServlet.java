package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // リクエストパラメータの入力項目を取得
        String userName = request.getParameter("userName");
     	String birthDate= request.getParameter("birthDate");
     	String address= request.getParameter("address");
     	String loginId = request.getParameter("loginId");
     	String password = request.getParameter("password");
    	String passwordConf = request.getParameter("passwordConf");


     	// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
     	UserDao userDao = new UserDao();
     	User user = userDao.allUserLoginId(loginId);

     	/** 登録できない場合 **/
		if (user != null || !password.equals(passwordConf) || userName.equals("") || birthDate.equals("") ||
				address.equals("") || loginId.equals("") || password.equals("") || passwordConf.equals("")) {

		// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力した内容は正しくありません");

		// リクエストスコープに入力した値をセット
			request.setAttribute("userName",userName);
			request.setAttribute("birthDate",birthDate);
			request.setAttribute("address",address);
			request.setAttribute("loginId",loginId);

		// userCreate.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 登録できる場合 **/

		// ユーザ情報の登録する
		UserDao userDao1 = new UserDao();
		User userInfo = userDao1.UserInfo(userName,birthDate,address,loginId,password);

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("IndexServlet");
	}



}
