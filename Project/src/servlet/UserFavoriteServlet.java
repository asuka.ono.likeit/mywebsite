package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.FavoriteDao;
import model.Favorite;

/**
 * Servlet implementation class UserFavoriteServlet
 */
@WebServlet("/UserFavoriteServlet")
public class UserFavoriteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserFavoriteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		try {
			// リクエストパラメータの文字コードを指定
	        request.setCharacterEncoding("UTF-8");

			//1ページに表示する旅行先数
			final int maxTravelCount = 6;

			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer.parseInt(request.getParameter("pageNum") == null ? "1" : request.getParameter("pageNum"));

			/**お気に入り情報の取得*/
			//ユーザidの取得
			String userId = request.getParameter("userId");

			// ユーザidと旅行先idからお気に入り情報を取得
			ArrayList<Favorite> favoriteList = FavoriteDao.getUserfavoriteTravel(userId,pageNum,maxTravelCount);

			// 総ページ数を取得
			double travelCount = FavoriteDao.getTravelCount(userId);
			int pageMax = (int) Math.ceil(travelCount / maxTravelCount);

			//総アイテム数
			request.setAttribute("travelCount", (int) travelCount);
			// 総ページ数
			request.setAttribute("pageMax", pageMax);
			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("favoriteList", favoriteList);


			//お気に入り0件の場合
			if(favoriteList == null || favoriteList.size() == 0) {

				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "お気に入り情報がありません");

				// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userFavorite.jsp");
				dispatcher.forward(request, response);
			}

				// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userFavorite.jsp");
				dispatcher.forward(request, response);


		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
