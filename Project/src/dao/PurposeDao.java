package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.PurposeTable;

public class PurposeDao {

	public static ArrayList<PurposeTable> getPurposeList() throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM purpose");

			ResultSet rs = st.executeQuery();

			ArrayList<PurposeTable> purposeList = new ArrayList<PurposeTable>();
			while (rs.next()) {
				PurposeTable purpose = new PurposeTable();
				purpose.setId(rs.getInt("id"));
				purpose.setPurpose(rs.getString("purpose"));
				purposeList.add(purpose);
			}


			return purposeList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
