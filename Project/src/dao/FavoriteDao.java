package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Favorite;

public class FavoriteDao {


	//お気に入り旅行先の登録
		public Favorite favoriteAdd(String userId,String travelId) {

			Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // INSERT文を準備
	            String sql = "INSERT INTO favorite"
	            		+ "(user_id, travel_id, create_date)"
	            		+ " VALUES (?, ?, now())";


	            // INSERTを実行する
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, userId);
	            pStmt.setString(2, travelId);

	            int result = pStmt.executeUpdate();

	            return null;
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
			} finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }


	//お気に入り旅行先の確認
			public Favorite favoriteConf(String userId,String travelId) {

				Connection conn = null;
		        try {
		            // データベースへ接続
			            conn = DBManager.getConnection();

			        // SELECT文を準備
			            String sql = "SELECT * FROM favorite"
		            			+ " WHERE user_id = ?"
		            			+ " AND travel_id = ?";


		            // SELECTを実行する
		            PreparedStatement pStmt = conn.prepareStatement(sql);
		            pStmt.setString(1, userId);
		            pStmt.setString(2, travelId);
		            ResultSet rs = pStmt.executeQuery();

		            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
		            if (!rs.next()) {
		                return null;
		            }

		            // 必要なデータのみインスタンスのフィールドに追加
		            int id = rs.getInt("id");

		            return new Favorite(id);
		        } catch (SQLException e) {
		            e.printStackTrace();
		            return null;
				} finally {
		            // データベース切断
			            if (conn != null) {
		                try {
		                    conn.close();
		                } catch (SQLException e) {
		                    e.printStackTrace();
		                    return null;
		                }
		            }
		        }
		    }


		//お気に入り旅行先の確認
			public static ArrayList<Favorite> getUserfavoriteTravel(String userId,int pageNum,int maxTravelCount) throws SQLException{
				Connection con = null;
				PreparedStatement st = null;
				try {
					int startTravelNum = (pageNum - 1) * maxTravelCount;
					con = DBManager.getConnection();

					st = con.prepareStatement("SELECT * FROM favorite"
											+ " JOIN travel_info"
											+ " ON favorite.travel_id = travel_info.id"
											+ " WHERE favorite.user_id = ?"
											+ " ORDER BY favorite.id ASC LIMIT ?,?");

		            st.setString(1, userId);
		            st.setInt(2, startTravelNum);
					st.setInt(3, maxTravelCount);

					ResultSet rs = st.executeQuery();

					ArrayList<Favorite> favoriteList = new ArrayList<Favorite>();
					while (rs.next()) {
						Favorite favorite = new Favorite();
						favorite.setId(rs.getInt("travel_info.id"));
						favorite.setTravelName(rs.getString("travel_info.travel_name"));
						favorite.setAreaId(rs.getInt("travel_info.area_id"));
						favorite.setPurposeId(rs.getInt("travel_info.purpose_id"));
						favorite.setAddress(rs.getString("travel_info.address"));
						favorite.setAccess(rs.getString("travel_info.access"));
						favorite.setDetail(rs.getString("travel_info.detail"));
						favorite.setFileName(rs.getString("travel_info.file_name"));
						favoriteList.add(favorite);
					}


					return favoriteList;
				} catch (SQLException e) {
					System.out.println(e.getMessage());
					throw new SQLException(e);
				} finally {
					if (con != null) {
						con.close();
					}
				}
			}


			// お気に入り旅行先総数を取得
			public static double getTravelCount(String userId) throws SQLException {
				Connection con = null;
				PreparedStatement st = null;
				try {
					con = DBManager.getConnection();
					st = con.prepareStatement("SELECT COUNT(*) as cnt FROM favorite"
											+ " WHERE user_id LIKE ?");

					 st.setString(1, userId);


					ResultSet rs = st.executeQuery();
					double coung = 0.0;
					while (rs.next()) {
						coung = Double.parseDouble(rs.getString("cnt"));
					}
					return coung;
				} catch (Exception e) {
					System.out.println(e.getMessage());
					throw new SQLException(e);
				} finally {
					if (con != null) {
						con.close();
					}
				}
			}


			//お気に入り旅行先の削除
			public Favorite userFavoriteDelete(String userId,String travelId) {

				Connection conn = null;
		        try {
		            // データベースへ接続
			            conn = DBManager.getConnection();

			        // DELETE文を準備
			            String sql = "DELETE FROM favorite"
		            			+ " WHERE user_id = ?"
		            			+ " AND travel_id = ?";


		            // DELETEを実行する
		            PreparedStatement pStmt = conn.prepareStatement(sql);
		            pStmt.setString(1, userId);
		            pStmt.setString(2, travelId);
		            int result = pStmt.executeUpdate();

		            return null;
		        } catch (SQLException e) {
		            e.printStackTrace();
		            return null;
				} finally {
		            // データベース切断
			            if (conn != null) {
		                try {
		                    conn.close();
		                } catch (SQLException e) {
		                    e.printStackTrace();
		                    return null;
		                }
		            }
		        }
		    }


}
