package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public User LoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";


            // password暗号化
            //ハッシュを生成したい元の文字列
            String source = password;

            //ハッシュ生成前にバイト配列に置き換える際のCharset
            Charset charset = StandardCharsets.UTF_8;

            //ハッシュアルゴリズム
            String algorithm = "MD5";

            //ハッシュ生成処理
            byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
            String Password = DatatypeConverter.printHexBinary(bytes);

            // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, Password);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            int id = rs.getInt("id");
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(id,loginIdData, nameData);



        } catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


	//ログインIDの確認（UserCreateServlet）
    public User allUserLoginId(String userId) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, userId);
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginId = rs.getString("login_id");

            return new User(0, loginId, null);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


  //ユーザ情報の登録（UserAddServlet）
    public User UserInfo(String userName,String birthDate,String address,String loginId,String password){

        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // INSERT文を準備
            String sql = "INSERT INTO user (name, address, birth_date, login_id, password, create_date, update_date) VALUES (?, ?, ?, ?, ?, now(), now())";

            // password暗号化
            //ハッシュを生成したい元の文字列
            String source = password;

            //ハッシュ生成前にバイト配列に置き換える際のCharset
            Charset charset = StandardCharsets.UTF_8;

            //ハッシュアルゴリズム
            String algorithm = "MD5";

            //ハッシュ生成処理
            byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
            String Password = DatatypeConverter.printHexBinary(bytes);



            // INSERTを実行する
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, userName);
            pStmt.setString(2, address);
            pStmt.setString(3, birthDate);
            pStmt.setString(4, loginId);
            pStmt.setString(5, Password);
            int result = pStmt.executeUpdate();

            return null;




        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return null;
		} finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


  //ユーザ情報の取得(UserDetailServlet)
    public User userDetail(String userid) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, userid);
            ResultSet rs = pStmt.executeQuery();

         // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

         // 必要なデータのみインスタンスのフィールドに追加
            int id = rs.getInt("id");
            String name = rs.getString("name");
            String address = rs.getString("address");
            Date birthDate = rs.getDate("birth_date");
            String loginId = rs.getString("login_id");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            return new User(id, name, address, birthDate, loginId, password, createDate, updateDate);



        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    //ユーザ情報の更新（UserUpdateServlet）
    public User updateUserInfo(String id,String userName,String address,String birthDate,String loginId,String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // UPDATE文を準備
            String sql = "UPDATE user SET name = ?, address = ?, birth_date = ?, login_id = ?, password = ?, update_date = now() WHERE id = ?";

         // password暗号化
            //ハッシュを生成したい元の文字列
            String source = password;

            //ハッシュ生成前にバイト配列に置き換える際のCharset
            Charset charset = StandardCharsets.UTF_8;

            //ハッシュアルゴリズム
            String algorithm = "MD5";

            //ハッシュ生成処理
            byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
            String Password = DatatypeConverter.printHexBinary(bytes);


             // UPDATEを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, userName);
            pStmt.setString(2, address);
            pStmt.setString(3, birthDate);
            pStmt.setString(4, loginId);
            pStmt.setString(5, Password);
            pStmt.setString(6, id);
            int rs = pStmt.executeUpdate();

         return null;


        } catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

}
