package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.AreaTable;

public class AreaDao {

	public static ArrayList<AreaTable> getAreaList() throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM area");

			ResultSet rs = st.executeQuery();

			ArrayList<AreaTable> areaList = new ArrayList<AreaTable>();
			while (rs.next()) {
				AreaTable area = new AreaTable();
				area.setId(rs.getInt("id"));
				area.setAreaName(rs.getString("area_name"));
				areaList.add(area);
			}


			return areaList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}



