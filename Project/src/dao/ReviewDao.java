package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Review;

public class ReviewDao {



	//口コミの登録
			public Review setReview(String userId,String travelId,String review){

				Connection conn = null;
		        try {
		            // データベースへ接続
		            conn = DBManager.getConnection();

		            // INSERT文を準備
		            String sql = "INSERT INTO review"
		            		+ "(user_id, travel_id, review, create_date)"
		            		+ " VALUES (?, ?, ?, now())";


		            // INSERTを実行する
		            PreparedStatement pStmt = conn.prepareStatement(sql);
		            pStmt.setString(1, userId);
		            pStmt.setString(2, travelId);
		            pStmt.setString(3, review);

		            int result = pStmt.executeUpdate();

		            return null;
		        } catch (SQLException e) {
		            e.printStackTrace();
		            return null;
				} finally {
		            // データベース切断
		            if (conn != null) {
		                try {
		                    conn.close();
		                } catch (SQLException e) {
		                    e.printStackTrace();
		                    return null;
		                }
		            }
		        }
		    }


	// 口コミの検索
	public static ArrayList<Review> getReviewList(String travelId) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM review"
									+ " JOIN user"
									+ " ON user.id = review.user_id"
									+ " WHERE travel_id = ?");

		    st.setString(1, travelId);
			ResultSet rs = st.executeQuery();

			ArrayList<Review> reviewList = new ArrayList<Review>();
			while (rs.next()) {
				Review review = new Review();
				review.setId(rs.getInt("id"));
				review.setUserName(rs.getString("user.name"));
				review.setReview(rs.getString("review"));
				review.setCreateDate(rs.getTimestamp("create_date"));
				reviewList.add(review);
			}


			return reviewList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	//口コミの削除
    public Review reviewDelete(String reviewId) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // DELETE文を準備
            String sql = "DELETE FROM review WHERE id = ?";

            // DELETEを実行する
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, reviewId);
            int result = pStmt.executeUpdate();

            return null;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


  //口コミの確認
	public static Review reviewConf(String travelId) {

		Connection conn = null;
        try {
            // データベースへ接続
	            conn = DBManager.getConnection();

	        // SELECT文を準備
	            String sql = "SELECT * FROM review"
            			+ " WHERE travel_id = ?";


            // SELECTを実行する
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, travelId);
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            int id = rs.getInt("id");

            return new Review(id);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
		} finally {
            // データベース切断
	            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

}
