package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.TravelInfo;

public class TravelInfoDao {

	//新規旅行先の登録
	public TravelInfo NewTravelInfo(String travelName,String areaId,String purposeId,String address,String access,String detail,String fileName) {

		Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // INSERT文を準備
            String sql = "INSERT INTO travel_info"
            		+ "(travel_name, area_id, purpose_id, address, access, detail, file_name, create_date, update_date)"
            		+ " VALUES (?, ?, ?, ?, ?, ?, ?, now(), now())";


            // INSERTを実行する
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, travelName);
            pStmt.setString(2, areaId);
            pStmt.setString(3, purposeId);
            pStmt.setString(4, address);
            pStmt.setString(5, access);
            pStmt.setString(6, detail);
            pStmt.setString(7, fileName);

            int result = pStmt.executeUpdate();

            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
		} finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


	// 旅行先の検索
	public static ArrayList<TravelInfo> getTravelInfoList(String keyword,String areaId,String purposeId,int pageNum,int maxTravelCount) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startTravelNum = (pageNum - 1) * maxTravelCount;
			con = DBManager.getConnection();

			if (keyword.length() == 0 && areaId.length() == 0 && purposeId.length() == 0) {

				// 全検索
				st = con.prepareStatement("SELECT * FROM travel_info ORDER BY id ASC LIMIT ?,? ");
				st.setInt(1, startTravelNum);
				st.setInt(2, maxTravelCount);
			}else {

				// キーワード、地域、目的での検索
				st = con.prepareStatement("SELECT * FROM travel_info"
										+ " WHERE travel_name LIKE ?"
										+ " AND area_id LIKE ?"
										+ " AND purpose_id LIKE ?"
										+ " ORDER BY id ASC LIMIT ?,?");

	            st.setString(1, "%" + keyword + "%");
	            st.setString(2, "%" + areaId + "%");
	            st.setString(3, "%" + purposeId + "%");
	            st.setInt(4, startTravelNum);
				st.setInt(5, maxTravelCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<TravelInfo> travelInfoList = new ArrayList<TravelInfo>();

			while (rs.next()) {
				TravelInfo travelInfo = new TravelInfo();
				travelInfo.setId(rs.getInt("id"));
				travelInfo.setTravelName(rs.getString("travel_name"));
				travelInfo.setAreaId(rs.getInt("area_id"));
				travelInfo.setPurposeId(rs.getInt("purpose_id"));
				travelInfo.setAddress(rs.getString("address"));
				travelInfo.setAccess(rs.getString("access"));
				travelInfo.setDetail(rs.getString("detail"));
				travelInfo.setFileName(rs.getString("file_name"));
				travelInfoList.add(travelInfo);
			}


			return travelInfoList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	 // 旅行先総数を取得
	public static double getTravelCount(String keyword,String areaId,String purposeId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT COUNT(*) as cnt FROM travel_info"
									+ " WHERE travel_name LIKE ?"
									+ " AND area_id LIKE ?"
									+ " AND purpose_id LIKE ?");

			st.setString(1, "%" + keyword + "%");
			st.setString(2, "%" + areaId + "%");
			st.setString(3, "%" + purposeId + "%");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



	// 旅行先情報の取得（全て）
		public static ArrayList<TravelInfo> getAllTravelInfoList() throws SQLException{
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();

				st = con.prepareStatement("SELECT * FROM travel_info ORDER BY RAND() LIMIT 3");
				ResultSet rs = st.executeQuery();

				ArrayList<TravelInfo> travelInfoList = new ArrayList<TravelInfo>();
				while (rs.next()) {
					TravelInfo travelInfo = new TravelInfo();
					travelInfo.setId(rs.getInt("id"));
					travelInfo.setTravelName(rs.getString("travel_name"));
					travelInfo.setAreaId(rs.getInt("area_id"));
					travelInfo.setPurposeId(rs.getInt("purpose_id"));
					travelInfo.setAddress(rs.getString("address"));
					travelInfo.setAccess(rs.getString("access"));
					travelInfo.setDetail(rs.getString("detail"));
					travelInfo.setFileName(rs.getString("file_name"));
					travelInfoList.add(travelInfo);
				}


				return travelInfoList;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}


		//旅行先情報の確認
	    public TravelInfo travelInfoDetail(String Id) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM travel_info"
	            			+ " JOIN area"
	            			+ " ON area.id = travel_info.area_id"
	            			+ " JOIN purpose"
	            			+ " ON purpose.id = travel_info.purpose_id"
	            			+ " WHERE travel_info.id = ?";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, Id);
	            ResultSet rs = pStmt.executeQuery();

	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	            if (!rs.next()) {
	                return null;
	            }

	            // 必要なデータのみインスタンスのフィールドに追加
	            int id = rs.getInt("id");
				String travelName = rs.getString("travel_name");
				String areaName = rs.getString("area.area_name");
				int areaId = rs.getInt("area.id");
				String purpose = rs.getString("purpose.purpose");
				int purposeId = rs.getInt("purpose.id");
				String address = rs.getString("address");
				String access = rs.getString("access");
				String detail = rs.getString("detail");
				String fileName = rs.getString("file_name");

	            return new TravelInfo(id, travelName, areaName, areaId, purpose, purposeId, address, access, detail, fileName, null, null);

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }


	  //旅行先情報の更新
	    public TravelInfo travelInfoUpdate(String id,String travelName,String areaId,String purposeId,String address,String access,String detail,String fileName) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // UPDATE文を準備
	            String sql = "UPDATE travel_info SET travel_name = ?,"
	            			+ " area_id = ?,"
	            			+ " purpose_id = ?,"
	            			+ " address = ?,"
	            			+ " access = ?,"
	            			+ " detail = ?,"
	            			+ " file_name = ?"
	            			+ " WHERE id = ?";

	            // UPDATEを実行する
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, travelName);
	            pStmt.setString(2, areaId);
	            pStmt.setString(3, purposeId);
	            pStmt.setString(4, address);
	            pStmt.setString(5, access);
	            pStmt.setString(6, detail);
	            pStmt.setString(7, fileName);
	            pStmt.setString(8, id);
	            int result = pStmt.executeUpdate();

	            return null;

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }


	  //旅行先情報の削除
	    public TravelInfo travelInfoDelete(String id) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // DELETE文を準備
	            String sql = "DELETE FROM travel_info WHERE id = ?";

	            // DELETEを実行する
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, id);
	            int result = pStmt.executeUpdate();

	            return null;

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }

}
